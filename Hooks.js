// Hooksの使用方法について
import React, {useState} from 'react';


//useStateは状態を管理するためのもの
const Hooks = () => {

    //[ 変数名, 関数名 ] = useState(初期値);
    const [count, setCount] = useState(0);

    return (
        <div>
            <button onClick={() => setCount(count+1)}>count {count}</button>
        </div>
    )
}

export default Hooks


Hooks tips

hooksを使用する際にcomponent名を小文字で記載するとコンパイルエラーになるので
必ず大文字で始まるように記載すること。(useState等)


MaterialUIインストール
    yarn add @material-ui/core
    yarn add @material-ui/icons


ReactHooksFormをインストール（使ってないのでアンインストールする必要があるかも）
    yarn add react-hook-form


Cookiesを使用する
    yarn add react-cookie


Routeを使用する
    yarn add react-router-dom


axiosを使用する(APIエンドポイントにアクセスする用)
    yarn add axios


MaterialUIにhref属性をつける方法
まず
    import { Link } from 'react-router-dom';
付けたいタグ内に
    component={Link} to="profile"



firebaseの使い方
まずログインをしてアプリを作成する
この時グーグル関係のアナリティクスは外しておく（使わないから）

次に作成したアプリの詳細へ行ってHostingのところへ飛ぶ
firebase login
firebase init
>Hosting
>Use an existing project
>build (ここではBuildファイルを読み込むため)
>Yes

で初期設定が完了する

次にdeploy時に参照するbuildファイルを作成する
>npm run build

最後にdeployする
>firebase deploy

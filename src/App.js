import React, {useState} from 'react';
import './App.css';

//追加
import './index.css';
import { Route, BrowserRouter } from 'react-router-dom';
import Signup from './components/user/Signup';
import Login from './components/user/Login';
import Top from './components/Top';
import Profile from './components/user/Profile';
import Author from './components/search/Author';
import Publisher from './components/search/Publisher';
import Category from './components/search/Category';
import { CookiesProvider } from 'react-cookie';
import Review from './components/book/Review';
import Registration from './components/book/Registration';
import Genre from './components/search/Genre';


//追加
import clsx from 'clsx';
import { fade, makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Container from '@material-ui/core/Container';


//追加
import FaceSharpIcon from '@material-ui/icons/FaceSharp';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import CreateIcon from '@material-ui/icons/Create';
import { Link } from 'react-router-dom';


//追加
import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';








//追加
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));




function App() {

    //追加
    const classes = useStyles();

    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const handleDrawerOpen = () => {
        setOpen(true);
    };
    const handleDrawerClose = () => {
        setOpen(false);
    };



	const [anchorEl, setAnchorEl] = useState(null);
	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};
	const handleClose = () => {
        setAnchorEl(null);
    };


    return (
        <div className={classes.root}>
            <React.StrictMode>
                <BrowserRouter>
                    <CookiesProvider>
                        <CssBaseline />
                        <AppBar
                            position="fixed"
                            className={clsx(classes.appBar, {
                            [classes.appBarShift]: open,
                            })}
                        >
                            <Toolbar>
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={handleDrawerOpen}
                                edge="start"
                                className={clsx(classes.menuButton, {
                                [classes.hide]: open,
                                })}
                            >
                                <MenuIcon />
                            </IconButton>
                            <IconButton color="inherit" component={Link} to="/">
                                FSComics
                            </IconButton>
                            </Toolbar>
                        </AppBar>
                        <Drawer
                            variant="permanent"
                            className={clsx(classes.drawer, {
                            [classes.drawerOpen]: open,
                            [classes.drawerClose]: !open,
                            })}
                            classes={{
                            paper: clsx({
                                [classes.drawerOpen]: open,
                                [classes.drawerClose]: !open,
                            }),
                            }}
                        >
                            <div className={classes.toolbar}>
                            <IconButton onClick={handleDrawerClose}>
                                {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                            </IconButton>
                            </div>
                            <Divider />
                            <List>
                                <ListItem button component={Link} to="/signup">
                                    <ListItemIcon><LockOutlinedIcon /></ListItemIcon>
                                    <ListItemText primary="Signup" />
                                </ListItem>
                                <ListItem button component={Link} to="review">
                                    <ListItemIcon>
                                        <Badge badgeContent={4} color="secondary">
                                            <MailIcon />
                                        </Badge>
                                    </ListItemIcon>
                                    <ListItemText primary="新規メール" />
                                </ListItem>
                                <ListItem button component={Link} to="review">
                                    <ListItemIcon>
                                        <Badge badgeContent={17} color="secondary">
                                            <NotificationsIcon />
                                        </Badge>
                                    </ListItemIcon>
                                    <ListItemText primary="通知" />
                                </ListItem>

                                {/* 修正箇所開始 */}
                                <ListItem button component={Link} to="review">
                                    <ListItemIcon>
                                        <FaceSharpIcon aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}/>
                                            <Menu
                                                id="simple-menu"
                                                anchorEl={anchorEl}
                                                keepMounted
                                                open={Boolean(anchorEl)}
                                                onClose={handleClose}
                                            >
                                                <MenuItem component={Link} to="/profile">Profile</MenuItem>
                                                <MenuItem component={Link} to="/Login">Login</MenuItem>
                                                <MenuItem component={Link} to="/Logout">Logout</MenuItem>
                                            </Menu>
                                    </ListItemIcon>
                                </ListItem>
                                {/* 修正箇所終了 */}
                                
                            </List>
                            <Divider />
                            <List>
                                <ListItem button component={Link} to="registration">
                                    <ListItemIcon><MenuBookIcon /></ListItemIcon>
                                    <ListItemText primary="本の登録" />
                                </ListItem>
                                <ListItem button component={Link} to="review">
                                    <ListItemIcon><CreateIcon /></ListItemIcon>
                                    <ListItemText primary="レビュー投稿" />
                                </ListItem>
                            </List>
                            <Divider />
                            <List>
                                <ListItem button component={Link} to="publisher">
                                    <ListItemIcon><MenuBookIcon /></ListItemIcon>
                                    <ListItemText primary="出版社" />
                                </ListItem>
                                <ListItem button component={Link} to="genre">
                                    <ListItemIcon><MenuBookIcon /></ListItemIcon>
                                    <ListItemText primary="ジャンル" />
                                </ListItem>
                                <ListItem button component={Link} to="category">
                                    <ListItemIcon><CreateIcon /></ListItemIcon>
                                    <ListItemText primary="カテゴリ" />
                                </ListItem>
                                <ListItem button component={Link} to="author">
                                    <ListItemIcon><FaceSharpIcon /></ListItemIcon>
                                    <ListItemText primary="作者" />
                                </ListItem>
                            </List>
                        </Drawer>
                        <main className={classes.content}>
                            <div className={classes.toolbar}></div>
                                {/* 内容をここに入れていきたい */}
                                <Route exact path="/" component={Top} />
                                <Route exact path="/signup" component={Signup} />
                                <Route exact path="/login" component={Login} />
                                <Route exact path="/profile" component={Profile} />
                                <Route path="/author" component={Author} />  
                                <Route path="/publisher" component={Publisher} />  
                                <Route path="/genre" component={Genre} />  
                                <Route path="/category" component={Category} />  
                                <Route path="/registration" component={Registration} />  
                                <Route path="/review" component={Review} />  
                                {/* <Route exact path="/signup" component={App} />
                                <Route exact path="/signup" component={App} /> */}
                        </main>
                    </CookiesProvider>
                </BrowserRouter>
            </React.StrictMode>
        </div>
    );
}

export default App;



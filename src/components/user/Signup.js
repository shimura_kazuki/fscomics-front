import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Select, TextField, MenuItem,  FormControl, InputLabel, Radio } from '@material-ui/core';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Card from '@material-ui/core/Card';


import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';




const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 3,
        backgroundColor: theme.palette.background.paper,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));  

const Signup = () => {
    const classes = useStyles();

    const [user, setUser] = useState({
        userName: '',
        password: '',
        sex: '',
        birthdayYear: '',
        birthdayMonth: '',
        birthdayDay: ''
        // aliveFlag: '',
        // adminFlag: '',
        // createdDate: '',
        // updatedDate: ''

    })

    const yearList = []
    for(let i = 1960; i <= 2020; i++ ) {
        yearList.push(i);
    }

    const [year, setYearList] = useState({
        value: yearList,
    })



    const monthList = []
    for(let i = 1; i <= 12; i++ ) {
        monthList.push(i);
    }

    const dayList = []
    for(let i = 1; i <= 31; i++ ) {
        dayList.push(i);
    }


    return (
        <React.Fragment>
            <main className={classes.content}>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Signup
                    </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="userName"
                            label="名前"
                            value={user.userName} 
                            name="userName"
                            autoComplete="userName"
                            onChange={(e) => setUser({...user, userName: e.target.value})} 
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            value={user.password} 
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={(e) => setUser({...user, password: e.target.value})}
                        />
                        <FormControl component="fieldset">
                        <FormLabel>性別</FormLabel>
                            <RadioGroup row aria-label="sex" margin="normal" name="sex" value={user.sex} onChange={(e) => setUser({...user, sex: e.target.value})}>
                                <FormControlLabel 
                                    value="1" 
                                    control={<Radio color="primary" />}
                                    label="男"
                                    labelPlacement="start"
                                />
                                <FormControlLabel
                                    value="2"
                                    control={<Radio color="primary" />}
                                    label="女"
                                    labelPlacement="start"
                                />
                                <FormControlLabel
                                    value="3"
                                    control={<Radio color="primary" />}
                                    label="その他"
                                    labelPlacement="start"
                                />
                            </RadioGroup>
                        </FormControl>
                        <div margin="normal" variant="outlined">
                            <FormControl className={classes.formControlBirthday}>
                                <InputLabel id="year">年</InputLabel>
                                <Select labelId="year" id="year" name="birthdayYear" value={user.birthdayYear} onChange={(e) => setUser({...user, birthdayYear: e.target.value})}>
                                    {
                                        yearList.map((value) => <MenuItem value={value}>{value}</MenuItem>)
                                    }
                                </Select>
                            </FormControl>
                            <FormControl className={classes.formControlBirthday}>
                                <InputLabel id="month">月</InputLabel>
                                <Select labelId="month" id="month" name="birthdayMonth" value={user.birthdayMonth} onChange={(e) => setUser({...user, birthdayMonth: e.target.value})}>
                                    {
                                        monthList.map((value) => <MenuItem value={value}>{value}</MenuItem>)
                                    }
                                </Select>
                            </FormControl>
                            <FormControl className={classes.formControlBirthday}>
                                <InputLabel id="day">日</InputLabel>
                                <Select labelId="day" id="day" name="birthdayDay" value={user.birthdayDay} onChange={(e) => setUser({...user, birthdayDay: e.target.value})}>
                                    {
                                        dayList.map((value) => <MenuItem value={value}>{value}</MenuItem>)
                                    }
                                </Select>
                            </FormControl>
                        </div>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Sign In
                        </Button>
                    </form>
                    <Card className={classes.root}>
                        <h3>name : {user.userName}</h3>
                        <h3>password : {user.password}</h3>
                        <h3>sex : {user.sex}</h3>
                        <h3>Year : {user.birthdayYear}</h3>
                        <h3>Month : {user.birthdayMonth}</h3>
                        <h3>Day : {user.birthdayDay}</h3>
                    </Card>
                </div>
            </main>
        </React.Fragment>
    )
}

export default Signup





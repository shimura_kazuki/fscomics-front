import React, { useState } from 'react';
// import { withCookies } from 'react-cookie';
// import axios from 'axios';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
// import CircularProgress from '@material-ui/core/CircularProgress'

// @material-ui/core components

import { Select, TextField, MenuItem, Container, FormControl, InputLabel } from '@material-ui/core';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';

const useStyles = makeStyles((theme) => ({
    content: {
        flexGrow: 3,
        padding: theme.spacing(6),
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },

    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
      },
}));


const Profile = () => {
    const classes = useStyles();

    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };


    const [user, setUser] = useState({
        userName: '',
        password: '',
        sex: '',
        birthdayYear: '',
        birthdayMonth: '',
        birthdayDay: ''
        // aliveFlag: '',
        // adminFlag: '',
        // createdDate: '',
        // updatedDate: ''

    })

    const yearList = []

    
    for(let i=1960; i < 2020; i++ ) {
        yearList.push(i);
    }

    const [year, setYearList] = useState({
        value: [1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
        2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
        2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020]
    })

    return (
        <React.Fragment>
            <Container component="main" maxWidth="xs">
                <main className={classes.content}>
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Profile
                        </Typography>
                        <form className={classes.form} noValidate>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="userName"
                                label="名前"
                                value={user.userName} 
                                name="userName"
                                autoComplete="userName"
                                autoFocus
                                onChange={(e) => setUser({...user, userName: e.target.value})} 
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                value={user.password} 
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                onChange={(e) => setUser({...user, password: e.target.value})}
                            />
                            <FormControl component="fieldset">
                            <FormLabel>性別</FormLabel>
                                <RadioGroup row aria-label="sex" name="sex" value={user.sex} onChange={(e) => setUser({...user, sex: e.target.value})}>
                                    <FormControlLabel 
                                        value="1" 
                                        control={<Radio color="primary" />}
                                        label="男"
                                        labelPlacement="start"
                                    />
                                    <FormControlLabel
                                        value="2"
                                        control={<Radio color="primary" />}
                                        label="女"
                                        labelPlacement="start"
                                    />
                                    <FormControlLabel
                                        value="3"
                                        control={<Radio color="primary" />}
                                        label="その他"
                                        labelPlacement="start"
                                    />
                                </RadioGroup>
                            </FormControl>
                            <FormControl className={classes.formControlBirthday}>
                                <InputLabel id="year">年</InputLabel>
                                <Select labelId="year" id="year"  value={user.birthdayYear} onChange={(e) => setUser({...user, birthdayYear: e.target.value})}>
                                    <MenuItem value={1}>1</MenuItem>
                                    <MenuItem value={2}>2</MenuItem>
                                    <MenuItem value={3}>3</MenuItem>
                                    <MenuItem value={4}>4</MenuItem>
                                    <MenuItem value={5}>5</MenuItem>
                                    <MenuItem value={6}>6</MenuItem>
                                    <MenuItem value={7}>7</MenuItem>
                                    <MenuItem value={8}>8</MenuItem>
                                    <MenuItem value={9}>9</MenuItem>
                                </Select>
                            </FormControl>
                            <FormControl className={classes.formControlBirthday}>
                                <InputLabel id="month">月</InputLabel>
                                <Select labelId="month" id="month"  value={user.birthdayMonth} onChange={(e) => setUser({...user, birthdayMonth: e.target.value})}>
                                    <MenuItem value={1}>1</MenuItem>
                                    <MenuItem value={2}>2</MenuItem>
                                    <MenuItem value={3}>3</MenuItem>
                                    <MenuItem value={4}>4</MenuItem>
                                    <MenuItem value={5}>5</MenuItem>
                                    <MenuItem value={6}>6</MenuItem>
                                    <MenuItem value={7}>7</MenuItem>
                                    <MenuItem value={8}>8</MenuItem>
                                    <MenuItem value={9}>9</MenuItem>
                                </Select>
                            </FormControl>
                            <FormControl className={classes.formControlBirthday}>
                                <InputLabel id="day">日</InputLabel>
                                <Select labelId="day" id="day"  value={user.birthdayDay} onChange={(e) => setUser({...user, birthdayDay: e.target.value})}>
                                    <MenuItem value={1}>1</MenuItem>
                                    <MenuItem value={2}>2</MenuItem>
                                    <MenuItem value={3}>3</MenuItem>
                                    <MenuItem value={4}>4</MenuItem>
                                    <MenuItem value={5}>5</MenuItem>
                                    <MenuItem value={6}>6</MenuItem>
                                    <MenuItem value={7}>7</MenuItem>
                                    <MenuItem value={8}>8</MenuItem>
                                    <MenuItem value={9}>9</MenuItem>
                                </Select>
                            </FormControl>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            >
                                Update
                            </Button>
                            </form>
                    </div>
                </main>
            </Container>
        </React.Fragment>
    )
}

export default Profile

import React, {useState} from 'react';

import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import FaceSharpIcon from '@material-ui/icons/FaceSharp';

import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
    grow: {
      	flexGrow: 1,
	},
	root: {
		display: 'flex',
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
	},
	link: {
		listStyle: 'none'
	},
}));
  

const Header = () => {
	const classes = useStyles();
	const [anchorEl, setAnchorEl] = useState(null);

	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};
	const handleClose = () => {
        setAnchorEl(null);
    };


  	return (
		<AppBar position="fixed" className={classes.appBar}>
			<Toolbar>
				<Grid container spacing={0}>
					<Grid item xs={2}  container direction="row" justify="flex-start" alignItems="center">
						<IconButton color="inherit" component={Link} to="/">
							FSComics
						</IconButton>
					</Grid>
					<Grid item xs={10} container direction="row" justify="flex-end" alignItems="center">
						<IconButton color="inherit" component={Link} to="/signup">
							<LockOutlinedIcon />
						</IconButton>
						<IconButton color="inherit">
							<Badge badgeContent={4} color="secondary">
								<MailIcon />
							</Badge>
						</IconButton>
						<IconButton color="inherit">
							<Badge badgeContent={17} color="secondary">
								<NotificationsIcon />
							</Badge>
						</IconButton>
						<IconButton color="inherit">
							<FaceSharpIcon aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}/>
								<Menu
									id="simple-menu"
									anchorEl={anchorEl}
									keepMounted
									open={Boolean(anchorEl)}
									onClose={handleClose}
								>
									<MenuItem component={Link} to="/profile">Profile</MenuItem>
									<MenuItem component={Link} to="/Login">Login</MenuItem>
									<MenuItem component={Link} to="/Logout">Logout</MenuItem>
								</Menu>
						</IconButton>
					</Grid>
				</Grid>
			</Toolbar>
      	</AppBar>
  	);
}

export default Header

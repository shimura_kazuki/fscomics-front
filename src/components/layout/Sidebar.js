import React, {useState} from 'react';

import { fade, makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import FaceSharpIcon from '@material-ui/icons/FaceSharp';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import CreateIcon from '@material-ui/icons/Create';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import { Link } from 'react-router-dom';

import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexShrink: 0,
    },
    title: {
        fontFamily: 'cursive',
        fontSize: '2.9rem',
        marginTop: '30px',
        marginBottom: '10px',
        color: 'blue'
    },

    search: {
		position: 'relative',
		borderRadius: theme.shape.borderRadius,
		backgroundColor: fade(theme.palette.common.white, 0.15),
		'&:hover': {
			backgroundColor: fade(theme.palette.common.white, 0.25),
		},
		marginRight: theme.spacing(2),
		marginLeft: 0,
		width: '100%',
		[theme.breakpoints.up('sm')]: {
			marginLeft: theme.spacing(3),
			width: 'auto',
		},
	},
	searchIcon: {
		padding: theme.spacing(0, 2),
		height: '100%',
		position: 'absolute',
		pointerEvents: 'none',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	inputRoot: {
		color: 'inherit',
	},
	inputInput: {
		padding: theme.spacing(1, 1, 1, 0),
		// vertical padding + font size from searchIcon
		paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
		transition: theme.transitions.create('width'),
		width: '100%',
		[theme.breakpoints.up('md')]: {
			width: '20ch',
		},
	},


}));


const Sidebar = () => {
    const classes = useStyles();

    return (
        <div >
            <Toolbar />
            <div >
                <List>
                    <ListItem button component={Link} to="registration">
                        <ListItemIcon><MenuBookIcon /></ListItemIcon>
                        <ListItemText primary="本の登録" />
                    </ListItem>
                    <ListItem button component={Link} to="review">
                        <ListItemIcon><CreateIcon /></ListItemIcon>
                        <ListItemText primary="レビュー投稿" />
                    </ListItem>
                </List>
            <Divider />
                <List>
                    <ListItem>
                        <ListItemIcon><ArrowDownwardIcon /><MenuBookIcon />検索条件<MenuBookIcon /></ListItemIcon>
                    </ListItem>
                    <Divider />
                        <div className={classes.search}>
							<div className={classes.searchIcon}>
							<SearchIcon />
							</div>
							<InputBase
							placeholder="Search…"
							classes={{
								root: classes.inputRoot,
								input: classes.inputInput,
							}}
							inputProps={{ 'aria-label': 'search' }}
							/>
						</div>
                    <ListItem button component={Link} to="publisher">
                        <ListItemIcon><MenuBookIcon /></ListItemIcon>
                        <ListItemText primary="出版社" />
                    </ListItem>
                    <ListItem button component={Link} to="genre">
                        <ListItemIcon><MenuBookIcon /></ListItemIcon>
                        <ListItemText primary="ジャンル" />
                    </ListItem>
                    <ListItem button component={Link} to="category">
                        <ListItemIcon><CreateIcon /></ListItemIcon>
                        <ListItemText primary="カテゴリ" />
                    </ListItem>
                    <ListItem button component={Link} to="author">
                        <ListItemIcon><FaceSharpIcon /></ListItemIcon>
                        <ListItemText primary="作者" />
                    </ListItem>
                </List>
            </div>
        </div>
    )
}

export default Sidebar
